
<div class="header">
	<div class="container" style="overflow: hidden; width: 100%;">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-md-4 col-lg-4">
				<div style="float:left; display:block;"><a href="/"><img src="images/hdLogo.png" class="logo" alt="HD"></a></div>
				<div style="float:left; display:block;"><a href="/"><img class="img_logo_hdays17 img-responsive" src="assets/images/logoharleydaysBLANCO_2017_ok.png" alt="Logo Harley Days 17"></a></div>
			</div>
			<div class="col-xs-11 col-sm-11 col-md-7 col-lg-6">
				<div class="navigation">
					<div id="navigation">
						<ul>
							<li class="menu_options prog" id="btn_menu_programa"><a href="/#programa" title="Programa">Programa</a>
							</li>
							<li class="menu_options rod"><a href="rodada.php" title="Rodada">Rodada</a>
							</li>
							<li class="menu_options bol"><a href="boletos.php" title="Boletos">Boletos</a>
							</li>
							<li class="menu_options sede"><a href="sede.php" title="Lugar">Sede</a>
							</li>
							<li class="menu_options talen"><a href="talento.php" title="Talento">Talento</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-1 col-md-1 col-lg-2">
            	<ul class="controles_sociales">
                    <li id="btn_share" class="btn_social_normal">SHARE</li>
                	<li id="btn_follow" class="btn_social_normal">FOLLOW</li>
                </ul>
				<ul id="logos_social">
					<li><a href="https://twitter.com/HarleyMexico" target="_blank" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_social_twitter','','assets/images/twiter_icon_over.png',1)"><img src="assets/images/twiter_icon.png" alt="Twitter" id="btn_social_twitter"></a>
					</li>
					<li><a href="https://www.instagram.com/harleydavidsonmexico/" target="_blank" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_social_instagram','','assets/images/instagram_icon_over.png',1)"><img src="assets/images/instagram_icon.png" alt="Instagram" id="btn_social_instagram"></a>
					</li>
					<li><a href="https://www.facebook.com/HarleyDavidsonMx" target="_blank" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_social_facebook','','assets/images/facebook_icon_over.png',1)"><img src="assets/images/facebook_icon.png" alt="Facebook" id="btn_social_facebook"></a>
					</li>
				</ul>
				<ul id="logos_share">
					<li><a href="http://twitter.com/share?text=Agenda para el 25 de noviembre.&hashtags=HarleyDays17&url=http://<?php echo $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]; ?>" target="_blank" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_share_twitter','','assets/images/twiter_icon_over.png',1)"><img src="assets/images/twiter_icon.png" alt="Twitter" id="btn_share_twitter"></a>
					</li>
					<li><a href="https://www.facebook.com/sharer/sharer.php?u=http://<?php echo $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]; ?>" target="_blank" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_share_facebook','','assets/images/facebook_icon_over.png',1)"><img src="assets/images/facebook_icon.png" alt="Facebook" id="btn_share_facebook"></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- Principal content -->

<div id="hero-section-landscape"></div>