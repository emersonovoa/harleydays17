$( document ).ready(function() {
    
    $(window).load(function(){
        
        $('.preload').fadeOut('slow',function(){$(this).remove();});
        
        setTimeout(function(){
            $('body').removeClass('preloading').addClass('loaded');

            if ($("body").hasClass("play")) {
                guidaoSize();

                setTimeout(function(){
                    $(window).resize();
                    guidaoSize();
                      $( ".moto" ).animate({
                        opacity: 1
                      }, 500, function() {
                        // Animation complete.
                      });
                }, 1000);
            }

            if ($("body").hasClass("home")) {
                init();
                setTimeout(function(){
                    liga();
                }, 3000);

                $('.url').click(function() {
                    playHarley();
                });
            }
        },1000);
        
    });
    
});

$( window ).resize(function() {
    if ($("body").hasClass("play")) {
        guidaoSize();
    }
});

var audioPath = "sound/";
var sounds = [
    {id:"Liga", src:"liga.mp3"},
    {id:"Loop", src:"loop.mp3"},
    {id:"Acelera", src:"acelera.mp3"}
];

function init() {
    // create an array and audioPath (above)
    createjs.Sound.registerSounds(sounds, audioPath);
}

var degPonteiro = "-35deg";     //valor inicial
var degGuidao = "0deg";         //valor inicial


function guidaoSize() {
    var tanqueH = $('.tanqueimg').height(),
        guidao = $('.guidao'),
        guidaoH = $('.guidao .guidaoimg').height(),
        ponteiro = $('.ponteiroimg'),
        ponteiroH = $('.ponteiroimg').height(),
        windowH = $(window).height();

    $('.inner-guidao').css({ 'transform': 'rotate(' + degGuidao + ')', 'margin-top': (windowH - tanqueH - (guidaoH / 2.2)) });
    ponteiro.css({ 'transform': 'rotate(' + degPonteiro + ')', 'margin-top': ((guidaoH / 3.8) - (ponteiroH / 2)) });
    $('.inner-guidao').height(ponteiroH);
};


function liga(){
        
    createjs.Sound.play('Liga');
    
    $('.luzapagada').fadeOut('fast');
    $('.flare').css({'animation-name': 'flareLigar','animation-duration':'2s','animation-iteration-count':'1'});
    setTimeout(function(){
        $('.flare').css({'animation-name': 'flare','animation-duration':'0.75s','animation-iteration-count':'infinite'});
        createjs.Sound.play('Loop');
        playLoop();
    }, 2000);
};

function playHarley(){
    $('.flare').css({'animation-name': ''});
    $('.flare').css({'opacity':'1'});
    
    
    playLoop = undefined;
    createjs.Sound.play('Acelera');
    
    setTimeout(function(){
        $('.container').css({'opacity':'0'});
        $('.brand').css({'opacity':'0'});
        $('.brand_right').css({'opacity':'0'});
        setTimeout(function(){
            window.location.href = './play.html';
        },1000);
    }, 3000);   
}

var playLoop = function(){
    var refreshIntervalId = setInterval(function(){
        createjs.Sound.play('Loop');
    },2880);
}