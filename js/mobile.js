// Find matches
var mql = window.matchMedia("(orientation: portrait)");

// Add a media query change listener
mql.addListener(function(m) {
  if(!m.matches) {
    $( ".header" ).hide();
    $( ".hero-section" ).hide();
    $( ".share" ).hide();
    $( ".footer" ).hide();
    $( "#hero-section-landscape" ).load( "landscape.html" );
  } else {    
    $( ".header" ).show();
    $( ".hero-section" ).show();    
    $( ".share" ).show();
    $( ".footer" ).show();
    $( "#hero-section-landscape" ).empty();

    var url = window.location.href;
    console.log("URL: " + url);
    var strIndex = "index.php";
    if(url.indexOf(strIndex) != -1){
      $( ".header" ).hide();
      $( ".hero-section" ).hide();
      $( ".share" ).hide();
      $( ".footer" ).hide();
      $( ".hero-section" ).hide();
      window.location.reload();
    }

  }
});

var url = window.location.href;
console.log("URL: " + url);
var strIndex = "index.php";
/*if(url.indexOf(strIndex) != -1){*/
if(false){

  var videoFileDesktop = 'assets/videos/intro_horizontal.mp4';
  var videoFileMobile  = 'assets/videos/intro_vertical.mp4';      


  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $('#divVideo video source').attr('src', videoFileMobile);
    $("#divVideo video")[0].load();    
  } else {
    $('#divVideo video source').attr('src', videoFileDesktop);
    $("#divVideo video")[0].load();    
  }

}