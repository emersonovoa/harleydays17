<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,target-densitydpi=device-dpi, user-scalable=no"/>
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<!-- made by www.metatags.org -->
	<!-- made by www.metatags.org -->
	<meta name="description" content="Sitio de la fiesta anual Harley-Davidson en México."/>
	<meta name="keywords" content="Harley, fiesta, celebración, motocicleta, moto, bike, harley-davidson, Autódromo Hermanos Rodríguez, Matute, Enjambre, Venta, RUEDA DE LA FORTUNA, SKY DIVING, PRUEBAS DE MANEJO, CONFERENCIAS, TIENDAS, TATUAJES, BARBER SHOP, BEAUTY SALON, comida"/>
	<meta name="author" content="MullenLowe Mexico">
	<meta name="robots" content="index, follow">
	<meta name="revisit-after" content="1 month">
	<link rel="icon" href="images/favicon.ico">
	<!-- METATAGS FACEBOOK -->
	<meta property="og:image" content="http://mexicoharleydays.com/assets/images/HDays_FB_hero.jpg"/>
	<meta property="og:title" content="Inicio - Harley Days &trade; 2017"/>
	<meta property="og:description" content="Sitio de la fiesta anual Harley-Davidson en México."/>
	<meta property="og:site_name" content="Harley Days 17"/>
	<meta property="og:url" content="http://mexicoharleydays.com"/>
	<meta property="og:type" content="website"/>
	<title>Programa - Harley Days &trade; 2017</title>
	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i%7cMontserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/jquery.bxslider.css" rel="stylesheet"/>
	<!-- Style -->
	<link href="css/intro.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<style>
		.header{
			display: none;
		}
		.footer{
			display: none;
		}
	</style>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js "></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js "></script>
	<![endif]-->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<?php include('_head_codes.php'); ?>
	<script type="text/javascript">
		function MM_swapImgRestore() { //v3.0
			var i, x, a = document.MM_sr;
			for ( i = 0; a && i < a.length && ( x = a[ i ] ) && x.oSrc; i++ ) x.src = x.oSrc;
		}

		function MM_preloadImages() { //v3.0
			var d = document;
			if ( d.images ) {
				if ( !d.MM_p ) d.MM_p = new Array();
				var i, j = d.MM_p.length,
					a = MM_preloadImages.arguments;
				for ( i = 0; i < a.length; i++ )
					if ( a[ i ].indexOf( "#" ) != 0 ) {
						d.MM_p[ j ] = new Image;
						d.MM_p[ j++ ].src = a[ i ];
					}
			}
		}

		function MM_findObj( n, d ) { //v4.01
			var p, i, x;
			if ( !d ) d = document;
			if ( ( p = n.indexOf( "?" ) ) > 0 && parent.frames.length ) {
				d = parent.frames[ n.substring( p + 1 ) ].document;
				n = n.substring( 0, p );
			}
			if ( !( x = d[ n ] ) && d.all ) x = d.all[ n ];
			for ( i = 0; !x && i < d.forms.length; i++ ) x = d.forms[ i ][ n ];
			for ( i = 0; !x && d.layers && i < d.layers.length; i++ ) x = MM_findObj( n, d.layers[ i ].document );
			if ( !x && d.getElementById ) x = d.getElementById( n );
			return x;
		}

		function MM_swapImage() { //v3.0
			var i, j = 0,
				x, a = MM_swapImage.arguments;
			document.MM_sr = new Array;
			for ( i = 0; i < ( a.length - 2 ); i += 3 )
				if ( ( x = MM_findObj( a[ i ] ) ) != null ) {
					document.MM_sr[ j++ ] = x;
					if ( !x.oSrc ) x.oSrc = x.src;
					x.src = a[ i + 2 ];
				}
		}
	</script>
</head>

<body onLoad="MM_preloadImages('assets/images/VE-MAS_-Fondo-Negro.png','assets/images/VE-MAS_-Fondo-Blanco.png','assets/images/Terxtura_Submenu.jpg')">
	<?php include("_header.php"); ?>

	<div id="intro" class="intro">
		<div class="contenedor_anim"></div>
		<div class="contenedor">
			<div class="btn_scroll"><a href="#programa" data-ancla="programa"><img src="assets/images/btn_scroll.png" class="img_down" id="btn_scroll_img"/></a>
			</div>
		</div>
	</div>

	<div class="hero-section programa_naranja">

		<div class="container">
			<!-- TITULO -->
			<div class="row row_titulo_anim" id="fila_programa">

				<?php /*?>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: right;">
					<div class="global_inner">
						<div class="row_inner">
							<a href="talento.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_mas_conciertos','','assets/images/VE-MAS_-Fondo-Negro.png',1)">
                         		<img data-titulo-anim="assets/animation/t_programa.gif" src="assets/images/spacer.gif" class="titulo_anim" style="width: 100%; max-width: 329px;">
                         	</a>
						
						</div>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 fila_contenido" style="text-align: left;">
					<div class="global_inner">
						<div class="row_inner">
							<div class="text_decoration_medium sincruz" style="text-align: left;">
								<br>INICIO DE ACTIVIDADES <br><span style="color: #FFF">11 A.M.</span>
							</div>
						</div>
					</div>
				</div>
				<?php */?>
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: right;">
					<div class="global_inner">
						<A name="programa" id="programa"></A>
						<div class="row_inner">
							<img data-titulo-anim="assets/animation/t_programa.gif" src="assets/images/spacer.gif" class="titulo_anim" style="width: 100%; max-width: 329px;">
						</div>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 fila_contenido" style="text-align: left;">
					<div class="global_inner">
						<div class="row_inner">
							<div class="text_decoration_medium sincruz title_program">
								<br>INICIO DE ACTIVIDADES <br><span style="color: #FFF">11:00 A.M.</span>
							</div>
						</div>
					</div>
				</div>

			</div>			

			<!-- Zona Harley -->
			<div class="row row_titulo_anim" id="filazona_harley">

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: right;">
					<div class="global_inner">
						<div class="row_inner">
							<?php /*?>
							<a href="zonaharley.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_mas_zonaharley','','assets/images/VE-MAS_-Fondo-Negro.png',1)">
                         		<img data-titulo-anim="assets/animation/s_p_zonaharley.gif" src="assets/images/spacer.gif" style="width: 100%; max-width: 400px;" class="titulo_anim">
                         	</a>
						
							<div id="link_zonaharley" class="ver_mas" style="text-align: right;">
								<a href="zonaharley.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_mas_zonaharley','','assets/images/VE-MAS_-Fondo-Negro.png',1)">
										<img src="assets/images/VE-MAS_-Fondo-Blanco.png" alt="+Más" id="btn_mas_zonaharley">
									</a>
							
							</div>
							<?php */?>
							<img data-titulo-anim="assets/animation/s_p_zonaharley.gif" src="assets/images/spacer.gif" style="width: 100%; max-width: 400px;" class="titulo_anim">
						</div>
					</div>
					<div class="global_inner fila_contenido">
						<div class="row_inner">
							<div class="text_decoration_small" style="text-align: right;">
								MODELOS 2018
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_medium" style="color: #fff;">
								VENTA DE MOTOCICLETAS
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_small" style="text-align: right;">
								CONFERENCIAS
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_medium" style="color: #fff;">
								PRUEBAS DE MANEJO
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-6 col-lg-6 fila_contenido" style="text-align: right;">
					<div class="global_inner">
						<div class="row_inner"><img src="assets/images/motocicleta.png" style="width: 100%; max-width: 400px;">
						</div>
						<div class="row_inner top">
							<div class="text_decoration_medium" style="color: #fff;">
								ZONA DE CUSTOMIZACI&Oacute;N
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_small">
								MODA 115 ANIVERSARIO
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_medium" style="color: #fff;">
								TIENDAS HARLEY&#174;
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_small">
								MODA HARLEY&#174;
							</div>
						</div>
					</div>
				</div>

			</div>

			<!-- Zona Extrema -->
			<div class="row row_titulo_anim" id="fila_zona_extrema">

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: right;">
					<div class="global_inner">
						<div class="row_inner">
							<?php /*?>
							<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_mas_zona_extrema','','assets/images/VE-MAS_-Fondo-Negro.png',1)">
                         		<img data-titulo-anim="assets/animation/s_p_zonaextrema.gif" src="assets/images/spacer.gif" style="width: 100%; max-width: 400px;" class="titulo_anim">
                         	</a>
						
							<div id="link_zona_extrema" class="ver_mas" style="text-align: right;">
								<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_mas_zona_extrema','','assets/images/VE-MAS_-Fondo-Negro.png',1)">
										<img src="assets/images/VE-MAS_-Fondo-Blanco.png" alt="+Más" id="btn_mas_zona_extrema">
									</a>
							
							</div>
							<?php */?>
							<img data-titulo-anim="assets/animation/s_p_zonaextrema.gif" src="assets/images/spacer.gif" style="width: 100%; max-width: 400px;" class="titulo_anim">
						</div>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 fila_contenido" style="text-align: left;margin-top: 40px;">
					<div class="global_inner">
						<div class="row_inner">
							<div class="text_decoration_small">
								<br>PRUEBA TU TEMPERAMENTO <br>Y DEMUESTRA TU VALENT&Iacute;A .
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_medium" style="color: #fff;">RUEDA DE LA FORTUNA</div>
						</div>
						<div class="row_inner" style="margin-top: 10px !important;">
							<div class="text_decoration_medium" style="color: #000;">SKY DIVING</div>
						</div>
						<div class="row_inner" style="margin-top: 10px !important;">
							<div class="text_decoration_medium" style="color: #fff;">SHOT FOR OVER</div>
						</div>
					</div>
				</div>

			</div>

		</div>

	</div>

	<div class="hero-section programa_emocion">

		<div class="container">

			<!-- Emocion -->
			<div class="row row_titulo_anim" id="fila_emocion">

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="global_inner">
						<div id="top_emocion"><img data-titulo-anim="assets/animation/T_Emocion_.gif" src="assets/images/spacer.gif" class="titulo_anim" style="width: 100%; max-width: 1280px;">
						</div>
					</div>
				</div>

			</div>

		</div>
	</div>

	<div class="hero-section programa_negro">

		<div class="container">

			<!-- Actividades -->
			<div class="row row_titulo_anim" id="fila_actividades">

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: right;">
					<div class="global_inner">

						<div class="row_inner">
							<?php /*?>
							<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_actividades','','assets/images/VE-MAS_-Fondo-Naranja.png',1)">
                         		<img data-titulo-anim="assets/animation/t_actividades.gif"  src="assets/images/spacer.gif" class="titulo_anim" style="width: 100%; max-width: 400px;">
                         	</a>
						
							<div id="link_conciertos" class="ver_mas" style="text-align: right;">
								<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_actividades','','assets/images/VE-MAS_-Fondo-Naranja.png',1)">
										<img src="assets/images/VE-MAS_-Fondo-Negro.png" alt="+Más" id="btn_actividades">
									</a>
							
							</div>
							<?php */?>
							<img data-titulo-anim="assets/animation/t_actividades.gif" src="assets/images/spacer.gif" class="titulo_anim" style="width: 100%; max-width: 400px;">
						</div>

					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 fila_contenido" style="text-align: center;">
					<div class="global_inner">
						<div class="row_inner">
							<div class="text_decoration_small" style="color: #e47625; text-align: left;">
								PORQUE NO S&Oacute;LO ES RODAR Y BAILAR...
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_medium" style="color: #fff;text-align: left;">TATUAJES</div>
						</div>
						<div class="row_inner" style="">
							<div class="text_decoration_medium" style="color: #e47625;text-align: left;">BARBER SHOP</div>
						</div>
						<div class="row_inner" style=";">
							<div class="text_decoration_medium" style="color: #fff;text-align: left;">BEAUTY SALON</div>
						</div>
					</div>
				</div>

			</div>

			<!-- Infantil  -->
			<div class="row row_titulo_anim" id="fila_zona_infantil">

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 fila_contenido" style="text-align: right;margin-top: 50px;">
					<div class="global_inner">
						<div class="row_inner">
							<div class="text_decoration_medium" style="color: #fff;text-align: right;">
								EURO BUNGEE
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_small custom_text" style="text-align: right; color: #e47625;">
								V FLYER
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_medium" style="color: #fff; text-align: right;">
								TATUAJES TEMPORALES
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_small custom_text" style="text-align: right; color: #e47625;">
								MANOS DE CERA
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_medium" style="color: #fff; text-align: right;">
								GREÑITAS PINTADAS
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_small custom_text" style="text-align: right; color: #e47625;">
								PINTA CARITAS
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: left;">
					<div class="global_inner">

						<div class="row_inner">
							<?php /*?>
							<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_zona_infantil','','assets/images/VE-MAS_-Fondo-Naranja.png',1)">
                         		<img data-titulo-anim="assets/animation/s_p_zonainfantil.gif"  src="assets/images/spacer.gif" class="titulo_anim" style="width: 100%; max-width: 400px;">
                         	</a>
						

							<?php /*?>
							<div id="link_conciertos" class="ver_mas" style="text-align: right;">
								<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_zona_infantil','','assets/images/VE-MAS_-Fondo-Naranja.png',1)">
										<img src="assets/images/VE-MAS_-Fondo-Negro.png" alt="+Más" id="btn_zona_infantil">
									</a>
							
							</div>
							<?php */?>
							<img data-titulo-anim="assets/animation/s_p_zonainfantil.gif" src="assets/images/spacer.gif" class="titulo_anim" style="width: 100%; max-width: 400px;">
						</div>
					</div>
				</div>

			</div>
			
			<!-- Promociones  -->
			<div class="row row_titulo_anim" id="fila_promociones">

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: right;">
					<div class="global_inner">
						<div class="row_inner" style="padding-top: 50px;">
							<div class="text_decoration_medium" style="color: #e47625; text-align: right;">
								LLEVA UN RECUERDO PARA SIEMPRE.
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: left;">
					<div class="global_inner">

						<div class="row_inner">
							<?php /*?>
							<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_promociones','','assets/images/VE-MAS_-Fondo-Naranja.png',1)">
                         		<img data-titulo-anim="assets/animation/s_p_promociones.gif" src="assets/images/spacer.gif" class="titulo_anim" style="width: 100%; max-width: 400px;">
                         	</a>
						

							<?php /*?>
							<div id="link_conciertos" class="ver_mas" style="text-align: right;">
								<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_promociones','','assets/images/VE-MAS_-Fondo-Naranja.png',1)">
										<img src="assets/images/VE-MAS_-Fondo-Negro.png" alt="+Más" id="btn_promociones">
									</a>
							
							</div>
							<?php */?>
							<img data-titulo-anim="assets/animation/s_p_promociones.gif" src="assets/images/spacer.gif" class="titulo_anim" style="width: 100%; max-width: 400px;">
						</div>

					</div>
				</div>

			</div>

			<!-- Zona de comida  -->
			<div class="row row_titulo_anim" id="fila_comida">

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: right; margin-top: 50px;">
					<div class="global_inner">

						<div class="row_inner">
							<?php /*?>
							<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_zona_comida','','assets/images/VE-MAS_-Fondo-Naranja.png',1)">
                         		<img data-titulo-anim="assets/animation/s_p_zonadecomida.gif"  src="assets/images/spacer.gif" class="titulo_anim" style="width: 100%; max-width: 400px;">
                         	</a>
						

							<?php /*?>
							<div id="link_conciertos" class="ver_mas" style="text-align: right;">
								<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_zona_comida','','assets/images/VE-MAS_-Fondo-Naranja.png',1)">
										<img src="assets/images/VE-MAS_-Fondo-Negro.png" alt="+Más" id="btn_zona_comida">
									</a>
							
							</div>
							<?php */?>
							<img data-titulo-anim="assets/animation/s_p_zonadecomida.gif" src="assets/images/spacer.gif" class="titulo_anim" style="width: 100%; max-width: 400px;">
							<div class="text_decoration_small" style="color: #FFF; text-align: right;">
								A PARTIR DE LAS 12:00 P.M.
							</div>
						</div>

					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 fila_contenido" style="text-align: center;">
					<div class="global_inner">
						<div class="row_inner">
							<img src="assets/images/motor.png" style="width: 100%; max-width: 400px;">
						</div>
					</div>
				</div>

			</div>

			<!-- Talento -->
			<div class="row row_titulo_anim" id="fila_conciertos">

				<?php /*?>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 fila_contenido" style="text-align: left;">
					<div class="global_inner">
						<div class="row_inner">
							<div class="text_decoration_medium sincruz" style="text-align: right; margin-top: 35px;">
								<br>LA FIESTA SE PRENDER&Aacute; CON ENERG&Iacute;A Y ACORDES A PARTIR DE LAS<span style="color: #FFF">7 P.M.</span>.
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: right;">
					<div class="global_inner">
						<div class="row_inner">
							<a href="talento.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_mas_conciertos','','assets/images/VE-MAS_-Fondo-Negro.png',1)">
                         		<img data-titulo-anim="assets/animation/s_p_conciertos.gif" src="assets/images/spacer.gif" class="titulo_anim" style="width: 100%; max-width: 400px;">
                         	</a>
						

							<div id="link_conciertos" class="ver_mas" style="text-align: center; margin-top: -75px;">
								<a href="talento.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_mas_conciertos','','assets/images/VE-MAS_-Fondo-Negro.png',1)">
										<img src="assets/images/VE-MAS_-Fondo-Blanco.png" alt="+Más" id="btn_mas_conciertos">
									</a>
							
							</div>
						</div>
					</div>
				</div>
				<?php */?>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 fila_contenido" style="text-align: left;">
					<div class="global_inner">
						<div class="row_inner" style="padding-top: 20px;">
							<div class="text_decoration_medium sincruz" style="text-align: right; margin-top: 35px;color: #FFF;">
								<br>LA FIESTA SE PRENDER&Aacute; CON ENERG&Iacute;A Y ACORDES A PARTIR DE LAS<span style="color: #FFF"> 7:00 P.M.</span>.
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: left;">
					<div class="global_inner">
						<div class="row_inner">
							<a href="talento.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_talento','','assets/images/VE-MAS_-Fondo-Naranja.png',1)">
								<img data-titulo-anim="assets/animation/talento-WHITE.gif" src="assets/images/spacer.gif" class="titulo_anim" style="width: 100%; max-width: 400px;">
							</a>

							<div id="link_conciertos" class="ver_mas" style="text-align: right;margin-top: -50px;">
								<a href="talento.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_talento','','assets/images/VE-MAS_-Fondo-Naranja.png',1)">
										<img src="assets/images/VE-MAS_-Fondo-Negro.png" alt="+Más" id="btn_talento">
									</a>							
							</div>

						</div>						
					</div>
				</div>

			</div>

			<!-- Reglamento  -->
			<div class="row row_titulo_anim" id="fila_reglamento">

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: right; margin-bottom:120px;">
					<div class="global_inner">

						<div class="row_inner">
							<a href="reglamento.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_reglamento','','assets/images/VE-MAS_-Fondo-Naranja.png',1)">
                         		<img data-titulo-anim="assets/animation/s_p_reglamento.gif" src="assets/images/spacer.gif" class="titulo_anim" style="width: 100%; max-width: 400px;">
                         	</a>
						

							<div id="link_conciertos" class="ver_mas" style="text-align: right;">
								<a href="reglamento.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('btn_reglamento','','assets/images/VE-MAS_-Fondo-Naranja.png',1)">
										<img src="assets/images/VE-MAS_-Fondo-Negro.png" alt="+Más" id="btn_reglamento">
									</a>
							
							</div>
						</div>

					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: left;">
					<div class="global_inner">
						<div class="row_inner" style="padding-top: 40px;">
							<div class="text_decoration_medium" style="color: #fff; text-align: left;">
								CHECA TODOS LOS LINEAMIENTOS.
							</div>

						</div>
					</div>
				</div>

			</div>

		</div>

	</div>

	<!-- End principal content -->
	<!-- footer-->
	<?php include("_footer.php"); ?>
	<script type="text/javascript">
		
		var timer_movi_btn;
		/* navigation selected option */
		$( '.prog a' ).addClass( 'btn_sel' );
			
		/* BOTON SCROLL */
		timer_movi_btn = setInterval( function () {
			$( ".contenedor" ).stop( true, true ).animate( {
					bottom: 50
				}, 800,
				function () {
					$( this ).stop( true, true ).animate( {
						bottom: 30
					}, 800 );
				} );
		}, 2000 );

		$( "#btn_scroll_img" ).click( function () {			
			/* hace visible el header y el footer */
			$('.header').show();				

			if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
				$('#sticky-wrapper').css('width','1280px');
				$('.header').css('width','1280px');
		    }

			$('.footer').show();			
			//$('.contenedor').hide();		
			$('.intro').hide();
			setTimeout(function(){
			  window.scrollTo(20, 20);
			}, 500);
		});
		
		/*controles titulos animados*/
		$( document ).ready( function () {
			$( window ).scroll( function () {
				var windowPos = $( window ).scrollTop();
				var windowHeight = window.innerHeight - 200;
				/*valora cada titulo*/
				$( '.row_titulo_anim' ).each( function ( e ) {
					var my_row_offset = $( this ).offset().top - windowPos;
					var img_titulo_anim = $( this ).find( '.titulo_anim' );
					var btn_ver_mas = $( this ).find( '.ver_mas' );
					var fila_contenido = $( this ).find( '.fila_contenido' );
					/*si es visible, cambia a la animacion*/
					if ( windowHeight >= my_row_offset && img_titulo_anim.attr( 'src' ) != img_titulo_anim.attr( 'data-titulo-anim' ) ) {
						img_titulo_anim.attr( 'src', img_titulo_anim.attr( 'data-titulo-anim' ) );
						btn_ver_mas.show();
						fila_contenido.fadeIn( 'slow' );

						/* hace visible el header y el footer */
						$('.header').show();				

						if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
							$('#sticky-wrapper').css('width','1280px');
							$('.header').css('width','1280px');
					    }

						$('.footer').show();
						$('.intro').hide();
						//$('.contenedor').hide();
						//window.scrollTo(10, 10);

					}
				} );
			} );

			/*la primera vez*/
			var windowPos = $( window ).scrollTop();
			var windowHeight = window.innerHeight - 200;
			var arr_titulos = [];
			/*valora cada titulo*/
			$( '.row_titulo_anim' ).each( function ( e ) {
				var my_row_offset = $( this ).offset().top - windowPos;
				var img_titulo_anim = $( this ).find( '.titulo_anim' );
				var btn_ver_mas = $( this ).find( '.ver_mas' );
				var fila_contenido = $( this ).find( '.fila_contenido' );
				/*si es visible, cambia a la animacion*/
				if ( windowHeight >= my_row_offset && img_titulo_anim.attr( 'src' ) != img_titulo_anim.attr( 'data-titulo-anim' ) ) {
					arr_titulos.push( $( this ) );

						/* hace visible el header y el footer */
						$('.header').show();				

						if(! /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
							$('#sticky-wrapper').css('width','1280px');
							$('.header').css('width','1280px');
					    }

						$('.footer').show();
						$('.intro').hide();
						//$('.contenedor').hide();
						window.scrollTo(10, 10);

				}
			} );
			/* encadena las acciones */

			if ( arr_titulos.length > 0 ) {
				var i = 0;
				var fn = function () {
					var element = $( arr_titulos[ i ] );
					var img_titulo_anim = element.find( '.titulo_anim' );
					var btn_ver_mas = element.find( '.ver_mas' );
					var fila_contenido = element.find( '.fila_contenido' );
					img_titulo_anim.attr( 'src', img_titulo_anim.attr( 'data-titulo-anim' ) );
					btn_ver_mas.show();
					fila_contenido.fadeIn( 'slow' );
					if ( ++i < arr_titulos.length ) {
						setTimeout( fn, 1800 );
					}
				};
				fn();
				
			}

		} );
	</script>
	<!-- /.footer-->
</body>

</html>