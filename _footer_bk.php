<div class="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-0">
				<div class="footer_ticketmaster">
					<a href="http://www.ticketmaster.com.mx/harley-days-2017-mexico-distrito-federal-25-11-2017/event/14005329AD0A4012?artistid=2414132&majorcatid=10004&minorcatid=25" target="_blank">
						<img src="assets/images/ticketmaster_corto.png" style="height: 50px; margin: 15px 0 15px 15px;">
					</a>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-7">
				<div class="copyright-content">
					<p>Harley-Davidson México / ©2001-2017 H.D. Todos los derechos reservados.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5">
				<div class="copyright-content-right">
					<a href="https://www.facebook.com/sharer/sharer.php?u=http://<?php echo $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]; ?>" target="_blank"><img class="footer_img_social" src="assets/images/F_bco_footer.png" alt="Facebook share" style="vertical-align:middle;">
                        </a>
					<a href="http://twitter.com/share?text=Agenda para el 25 de noviembre.&hashtags=HarleyDays17&url=http://<?php echo $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]; ?>" target="_blank"><img class="footer_img_social" src="assets/images/T_bco_footer.png" alt="Twitter share" style="vertical-align:middle;">
					</a>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/menumaker.js"></script>
<!-- sticky header -->
<script src="js/jquery.sticky.js"></script>
<script src="js/sticky-header.js"></script>
<script src="js/mobile.js"></script>
<script src="js/jquerymobile-swipeupdown.js"></script>
<script type="text/javascript">
	/* FUNCIONES MENU */
	var ban_btn_follow = false;
	var ban_btn_share = false;
	$( '#btn_follow' ).click( function () {
		if ( !ban_btn_follow ) {
			$( '#logos_social' ).show();
			$( '#logos_share' ).hide();
			$( this ).css( 'color', '#F07D00' );
			$( '.controles_sociales' ).css( 'margin-top', '15px' );
			$( '#btn_share' ).css( 'color', '#000' );
			ban_btn_follow = true;
		} else {
			ban_btn_follow = false;
			fn_controles_sociales_off();
		}
	} );
	$( '#btn_share' ).click( function () {
		if ( !ban_btn_share ) {
			$( '#logos_social' ).hide();
			$( '#logos_share' ).show();
			$( this ).css( 'color', '#F07D00' );
			$( '.controles_sociales' ).css( 'margin-top', '15px' );
			$( '#btn_follow' ).css( 'color', '#000' );
			ban_btn_share = true;
		} else {
			ban_btn_share = false;
			fn_controles_sociales_off();
		}
	} );
	var fn_controles_sociales_off = function () {
		$( '#logos_social' ).hide();
		$( '#logos_share' ).hide();
		$( '.controles_sociales' ).css( 'margin-top', '37px' );
		$( '#btn_share' ).css( 'color', '#999' );
		$( '#btn_follow' ).css( 'color', '#999' );
	};
	$( '.hero-section' ).click( function () {
		fn_controles_sociales_off();
	} );
	$( '.sticky-wrapper' ).click( function () {
		fn_controles_sociales_off();
	} );
	$( document ).keyup( function ( e ) {
		if ( e.keyCode == 27 ) { // escape key maps to keycode `27`
			// <DO YOUR WORK HERE>
			fn_controles_sociales_off();
		}
	} );
	/* FIN FUNCIONES MENU */


	$( window ).on( "navigate", function ( event, data ) {
		var direction = data.state.direction;
		if ( !!direction ) {
			alert( direction );
		}
	} );
</script>