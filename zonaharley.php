<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,target-densitydpi=device-dpi, user-scalable=no"/>
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<!-- made by www.metatags.org -->
	<!-- made by www.metatags.org -->
	<meta name="description" content="Sitio de la fiesta anual Harley-Davidson en México."/>
	<meta name="keywords" content="Harley, fiesta, celebración, motocicleta, moto, bike, harley-davidson, Autódromo Hermanos Rodríguez, Matute, Enjambre, Venta, RUEDA DE LA FORTUNA, SKY DIVING, PRUEBAS DE MANEJO, CONFERENCIAS, TIENDAS, TATUAJES, BARBER SHOP, BEAUTY SALON, comida"/>
	<meta name="author" content="MullenLowe Mexico">
	<meta name="robots" content="index, follow">
	<meta name="revisit-after" content="1 month">
	<link rel="icon" href="images/favicon.ico">
	<!-- METATAGS FACEBOOK -->
	<meta property="og:image" content="http://mexicoharleydays.com/assets/images/HDays_FB_hero.jpg"/>
	<meta property="og:title" content="Inicio - Harley Days &trade; 2017"/>
	<meta property="og:description" content="Sitio de la fiesta anual Harley-Davidson en México."/>
	<meta property="og:site_name" content="Harley Days 17"/>
	<meta property="og:url" content="http://mexicoharleydays.com"/>
	<meta property="og:type" content="website"/>
	<title>Zona Harley - Harley Days &trade; 2017</title>
	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i%7cMontserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Style -->
	<link href="css/style.css" rel="stylesheet">
	<link href="css/jquery.bxslider.css" rel="stylesheet"/>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js "></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js "></script>

<![endif]-->
	<?php include('_head_codes.php'); ?>
	<script type="text/javascript">
		function MM_swapImgRestore() { //v3.0
			var i, x, a = document.MM_sr;
			for ( i = 0; a && i < a.length && ( x = a[ i ] ) && x.oSrc; i++ ) x.src = x.oSrc;
		}

		function MM_preloadImages() { //v3.0
			var d = document;
			if ( d.images ) {
				if ( !d.MM_p ) d.MM_p = new Array();
				var i, j = d.MM_p.length,
					a = MM_preloadImages.arguments;
				for ( i = 0; i < a.length; i++ )
					if ( a[ i ].indexOf( "#" ) != 0 ) {
						d.MM_p[ j ] = new Image;
						d.MM_p[ j++ ].src = a[ i ];
					}
			}
		}

		function MM_findObj( n, d ) { //v4.01
			var p, i, x;
			if ( !d ) d = document;
			if ( ( p = n.indexOf( "?" ) ) > 0 && parent.frames.length ) {
				d = parent.frames[ n.substring( p + 1 ) ].document;
				n = n.substring( 0, p );
			}
			if ( !( x = d[ n ] ) && d.all ) x = d.all[ n ];
			for ( i = 0; !x && i < d.forms.length; i++ ) x = d.forms[ i ][ n ];
			for ( i = 0; !x && d.layers && i < d.layers.length; i++ ) x = MM_findObj( n, d.layers[ i ].document );
			if ( !x && d.getElementById ) x = d.getElementById( n );
			return x;
		}

		function MM_swapImage() { //v3.0
			var i, j = 0,
				x, a = MM_swapImage.arguments;
			document.MM_sr = new Array;
			for ( i = 0; i < ( a.length - 2 ); i += 3 )
				if ( ( x = MM_findObj( a[ i ] ) ) != null ) {
					document.MM_sr[ j++ ] = x;
					if ( !x.oSrc ) x.oSrc = x.src;
					x.src = a[ i + 2 ];
				}
		}
	</script>

	<style type="text/css">
		.container {
			overflow-x: hidden;
			width: 100%;
		}
	</style>

</head>

<body>
	<?php include("_header.php"); ?>
	<div id="hero-section-rodada" class="hero-section">
		<div class="container" style="padding-right: 1px !important;padding-left: 1px !important;">
			<div class="row">
				<div id="down" class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
					<div class="content-left-zonaharley">
						<div class="row_inner">
							<img src="assets/animation/t_s_zonaharley.gif" style="width: 100%; max-width: 269px;padding-left: 5px;">
						</div>
						<div class="row_inner">
							<div class="text_decoration_medium" style="text-align: left;font-size: 1.3em;">
								EXHIBICI&Oacute;N DE NUEVOS
							</div>
							<div class="text_decoration_medium sincruz" style="text-align: center;font-size: 1.7em">
								MODELOS 2018
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_medium" style="color: #fff;text-align: left;">
								VENTA DE MOTOCICLETAS
							</div>
							<div class="text_decoration_medium sincruz" style="color: #fff;font-size: 1.2em;">
								CON BENEFICIOS ESPECIALES
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_medium" style="text-align: left;">
								CONFERENCIAS
							</div>
							<div class="text_decoration_medium sincruz" style="color: #fff;">
								PRUEBAS DE MANEJO
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_medium" style="text-align: left;font-size: 1.3em;">
								ZONA DE MERCANC&Iacute;A GENERAL
							</div>
							<div class="text_decoration_medium sincruz" style="color: #fff;">
								TIENDAS HARLEY&#174;
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_small_normal" style="color: #FFF;padding-left: 5px;">
								Compra accesorios, conoce los nuevos modelos 2018 y pru&eacute;bate las nuevas colecciones de moda para rodar.
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_medium" style="color: #fff;text-align: left;">
								ZONA DE CUSTOMIZACI&Oacute;N
							</div>
							<div class="text_decoration_medium" style="text-align: center;font-size: 1.3em;">
								EXHIBICI&Oacute;N MOTOS CUSTOM
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_medium" style="text-align: left;">
								PARTES & ACCESORIOS&#174; ORIGINALES HARLEY-DAVIDSON
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_medium" style="color: #fff;text-align: left;">
								MODA 115 ANIVERSARIO
							</div>
						</div>
						<div class="row_inner">
							<div class="text_decoration_medium" style="text-align: center;">
								MODA HARLEY&#174;
							</div>
						</div>
					</div>

					<div class="content-right-zonaharley"></div>
				</div>

				<div id="top" class="col-lg-9 col-md-6 col-sm-6 col-xs-12">
					<div class="slider-zonaharley">
						<ul class="bxslider">
							<li><img src="assets/images/c_zonaharler_1.jpg" style="width: 100%;">
							</li>
							<li><img src="assets/images/c_zonaharler_2.jpg" style="width: 100%;">
							</li>
							<li><img src="assets/images/c_zonaharler_3.jpg" style="width: 100%;">
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--div class="share">
		<div class="container">

			<!-- share>
			<div class="row row_titulo_anim">
				              
                <div class="col-lg-4 col-md-4 col-sm-2 col-xs-2">
                  <div class="global_inner">
                  </div>
              	</div>
                <div class="col-lg-8 col-md-8 col-sm-10 col-xs-10">
                  <div class="global_inner">
                  		<div class="row_inner">
                        	<span class="share_texto" >
								COMPARTE  
							</span>
               	  		<a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//mexicoharleydays.com/rodada.php" target="_blank"><img src="images/facebook_icon.png" width="12%" alt="Facebook share" style="vertical-align:middle;">
                        </a>
               	  	<a href="#" target="_blank"><img src="images/instagram_icon.png" width="12%"  alt="Instagram" style="vertical-align:middle;" target="_blank"></a>
                  	<a href="https://twitter.com/intent/tweet?button_hashtag=HarleyDaysMx17&ref_src=twsrc%5Etfw" target="_blank"><img src="images/twiter_icon.png" width="12%" alt="Twitter share" style="vertical-align:middle;">
                        </a>
						</div>
                        
                  </div>
              	</div>

			</div>

		</div>
	</div-->
	<!-- End principal content -->
	<!-- footer-->
	<?php include("_footer.php"); ?>
	<script src="js/jquery.bxslider.min.js"></script>
	<script type="text/javascript">
		$( document ).ready( function () {
			$( '.bxslider' ).bxSlider();

			$( "#ticketmaster" ).click( function () {
				window.location = "http://www.ticketmaster.com.mx/";
			} );

		} );
	</script>
	<!-- /.footer-->
</body>

</html>