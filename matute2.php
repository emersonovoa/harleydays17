<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,target-densitydpi=device-dpi, user-scalable=no"/>
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<!-- made by www.metatags.org -->
	<!-- made by www.metatags.org -->
	<meta name="description" content="Sitio de la fiesta anual Harley-Davidson en México."/>
	<meta name="keywords" content="Harley, fiesta, celebración, motocicleta, moto, bike, harley-davidson, Autódromo Hermanos Rodríguez, Matute, Enjambre, Venta, RUEDA DE LA FORTUNA, SKY DIVING, PRUEBAS DE MANEJO, CONFERENCIAS, TIENDAS, TATUAJES, BARBER SHOP, BEAUTY SALON, comida"/>
	<meta name="author" content="MullenLowe Mexico">
	<meta name="robots" content="index, follow">
	<meta name="revisit-after" content="1 month">
	<link rel="icon" href="images/favicon.ico">
	<!-- METATAGS FACEBOOK -->
	<meta property="og:image" content="http://mexicoharleydays.com/assets/images/HDays_FB_hero.jpg"/>
	<meta property="og:title" content="Inicio - Harley Days &trade; 2017"/>
	<meta property="og:description" content="Sitio de la fiesta anual Harley-Davidson en México."/>
	<meta property="og:site_name" content="Harley Days 17"/>
	<meta property="og:url" content="http://mexicoharleydays.com"/>
	<meta property="og:type" content="website"/>
	<title>Matute - Harley Days &trade; 2017</title>
	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i%7cMontserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Style -->
	<link href="css/style.css" rel="stylesheet">
	<link href="css/jquery.bxslider.css" rel="stylesheet"/>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js "></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js "></script>

<![endif]-->
	<?php include('_head_codes.php'); ?>
	<script type="text/javascript">
		function MM_swapImgRestore() { //v3.0
			var i, x, a = document.MM_sr;
			for ( i = 0; a && i < a.length && ( x = a[ i ] ) && x.oSrc; i++ ) x.src = x.oSrc;
		}

		function MM_preloadImages() { //v3.0
			var d = document;
			if ( d.images ) {
				if ( !d.MM_p ) d.MM_p = new Array();
				var i, j = d.MM_p.length,
					a = MM_preloadImages.arguments;
				for ( i = 0; i < a.length; i++ )
					if ( a[ i ].indexOf( "#" ) != 0 ) {
						d.MM_p[ j ] = new Image;
						d.MM_p[ j++ ].src = a[ i ];
					}
			}
		}

		function MM_findObj( n, d ) { //v4.01
			var p, i, x;
			if ( !d ) d = document;
			if ( ( p = n.indexOf( "?" ) ) > 0 && parent.frames.length ) {
				d = parent.frames[ n.substring( p + 1 ) ].document;
				n = n.substring( 0, p );
			}
			if ( !( x = d[ n ] ) && d.all ) x = d.all[ n ];
			for ( i = 0; !x && i < d.forms.length; i++ ) x = d.forms[ i ][ n ];
			for ( i = 0; !x && d.layers && i < d.layers.length; i++ ) x = MM_findObj( n, d.layers[ i ].document );
			if ( !x && d.getElementById ) x = d.getElementById( n );
			return x;
		}

		function MM_swapImage() { //v3.0
			var i, j = 0,
				x, a = MM_swapImage.arguments;
			document.MM_sr = new Array;
			for ( i = 0; i < ( a.length - 2 ); i += 3 )
				if ( ( x = MM_findObj( a[ i ] ) ) != null ) {
					document.MM_sr[ j++ ] = x;
					if ( !x.oSrc ) x.oSrc = x.src;
					x.src = a[ i + 2 ];
				}
		}
	</script>

	<style type="text/css">
		.container {
			overflow-x: hidden;
			width: 100%;
		}
	</style>

</head>

<body onLoad="MM_preloadImages('assets/animation/t_s_talento_crop.gif', 'assets/images/back_programa_negro.jpg')">
	<?php include("_header.php"); ?>
	<div id="hero-section-talento_detalle" class="hero-section" style="background-image: url(assets/images/back_programa_negro.jpg) !important;">
		<div class="container" style="padding: 0px !important;">
			<div class="row">
				<div id="down" class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
					<div class="content-left-talento_detalle">
						<div class="row_inner">
							<img src="assets/animation/t_s_talento_crop.gif" style="width: 100%; max-width: 270px;">
						</div>
					</div>
					<div class="content-right-talento_detalle">
						<div class="row_inner">
							<img src="assets/images/Logo_Matute.png" style="width: 100%; max-width: 394px;">
						</div>
						<div class="row_inner">
							<div class="text_decoration_small_bandas_normal">
								Matute es una banda que rinde homenaje a la buena música de los 80 en inglés y en español. Fue formada en el 2007, bajo la dirección de Jorge D’Alessio. La banda se hace llamar "Matute" en honor a un personaje de "Don Gato y su pandilla". Este año revivirán a exponentes como: Baltimora, Hombres G, Miguel Mateos, entre otros, durante Harley Days™ México 2017.
							</div>
						</div>
					</div>
				</div>

				<div id="top" class="col-lg-9 col-md-6 col-sm-6 col-xs-12">
					<div class="slider-talento_detalle">
							<li><video fullscreen controls width="100%" poster="assets/images/thumb_matute.jpg">
									<source src="assets/videos/MATUTE_720.mp4" type="video/mp4"> Your browser does not support the video tag.
								</video>
							</li>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- End principal content -->
	<!-- footer-->
	<?php include("_footer.php"); ?>
	<script src="js/jquery.bxslider.min.js"></script>
	<script type="text/javascript">
		/* navigation selected option */
		$( '.talen a' ).addClass( 'btn_sel' );
		/* fin navigation selected option */

		$( document ).ready( function () {
			$( '.bxslider' ).bxSlider( {
				autoStart: true,
				auto: true,
				autoHover: true
			} );

			$( "#ticketmaster" ).click( function () {
				window.location = "http://www.ticketmaster.com.mx/";
			} );

		} );
	</script>
	<!-- /.footer-->
</body>

</html>